
import java.util.Scanner;

/**
 * Решение задачи №11
 * Необходиом написать метод, определяющий количество часов, минут и секунд в n сутках
 *
 * @author Андрюшин Дмитрий, 16ит18К
 */
public class Excersize11 {
    private static Scanner scanner = new Scanner(System.in);

    /**
     * Метод с помощью объекта {@link #scanner} получает число суток. Затем вычисляется число часов путём умножения кол-ва суток на 24
     * Следующий шаг - нахождения числа минут, умножив количество часов на 60, затем вычисляется кол-во секунд - умножив число минут на 60
     * Затем на экран выводятся вычисленные значения и выполнение метода завершается
     *
     * @param args массив строк {@link java.lang.String}, представляющих аргументы, переданные программе в командной строке/терминале
     */
    public static void main(String[] args) {
        System.out.print("Введите кол-во суток: ");
        int count = scanner.nextInt();
        int hours = count * 24;
        int minutes = hours * 60;
        int seconds = minutes * 60;
        System.out.printf("В %d сут. %d ч. %d мин. %d сек.", count, hours, minutes, seconds);
    }
}

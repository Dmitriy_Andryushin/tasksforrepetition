import java.util.Arrays;
import java.util.Scanner;

/**
 * Решение задачи №2
 * Требуется написать программу, увеличивающую заданный элемент массива на 10%
 *
 * @author Андрюшин Дмитрий, 16ИТ18К
 */
public class Excersize2 {
    /**
     * Объект типа {@link java.util.Scanner} позволяет считывать введённые с клавиатуры символы, числа и строки
     */
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int count = readCount();
        double[] array = new double[count];
        for (int i = 0; i < count; i++) {
            array[i] = Math.round(Math.random() * 10000.0) / 100.0;
        }

        int position = readPosition(count);
        System.out.printf("Массив до увеличения: %s%n", Arrays.toString(array));
        increase(array, position);
        System.out.printf("Массив после увеличения: %s%n", Arrays.toString(array));
    }

    /**
     * Возвращает кол-во элементов массива, полученное от пользователя с проверкой на правильность
     * Число элементов не может быть меньше 0 по логике и больше 1000 (для экономии памяти и предотвращения возможного перерасхода памяти на очень слабых машинах)
     *
     * @return кол-во элементов в массиве
     */
    public static int readCount() {
        System.out.print("Введите количество элементов массива: ");
        int count = scanner.nextInt();
        while (count < 0 || count > 1000) {
            System.out.print("Похоже, вы допустили ошибку при вводе числа. Попробуйте еще раз: ");
            count = scanner.nextInt();
        }
        return count;
    }

    /**
     * Возвращает номер увеличиваемого элемента в массиве. Принимает на вход количество элементов массива для определения границ диапазона возможных значений
     *
     * @param count количество элементов массива
     * @return номер увеличиваемого элемента в массие в диапазоне от 0 до count
     */
    public static int readPosition(int count) {
        System.out.print("Введите номер увеличиваемого элемента: ");
        int pos = scanner.nextInt();
        while (pos < 0 || pos >= count) {
            System.out.print("Похоже, вы допустили ошибку при вводе числа. Попробуйте еще раз: ");
            pos = scanner.nextInt();
        }
        return pos;
    }

    /**
     * Увеличивает элемент массива array с индексом pos на 10%. Если индекс больше или равен количеству элементов массива array будет выброшена исключительная ситуация {@link java.lang.UnsupportedOperationException}
     *
     * @param array массив элементов произвольной длины
     * @param pos   индекс увеличиваемого элемента
     * @throws UnsupportedOperationException если индекс pos больше или равен количеству элементов массива array
     */
    public static void increase(double[] array, int pos) throws UnsupportedOperationException {
        if (pos >= array.length)
            throw new UnsupportedOperationException("Нельяза выходить за пределы массива! Это плохо кончится! Длина массива: " + array.length + ", индекс: " + pos);

        array[pos] *= 1.1;
    }
}

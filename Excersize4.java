import java.util.Scanner;

public class Excersize4 {
    private static final double SPEED_OF_SOUND = 343.0;

    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите время между молнией и громом в секундах: ");
        double time = readCorrect();
        System.out.printf("За время %.3f сек. молния прошла %.3f м%n", time, time * SPEED_OF_SOUND);
    }

    public static double readCorrect() {
        double m = scanner.nextDouble();
        while(m < 0) {
            System.out.print("Похоже, вы допустили ошибку! Попробуйте еще раз: ");
            m = scanner.nextDouble();
        }
        return m;
    }
}

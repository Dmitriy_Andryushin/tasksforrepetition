import java.util.Scanner;

/**
 * Решение задачи №9
 * Требуется написать  метод, определяющий палиндромность числа
 *
 * @author Андрюшин Дмитрий, 16ит18К
 */
public class Excersize9 {

    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.print("Введите число : ");
        int number = scanner.nextInt();
        System.out.printf("Числом %s является палиндромом!%n", isPalindrome(Math.abs(number)) ? " " : " не ");
    }

    /**
     * Возвращает результат сравнения числа с тем же число, но записанным справа на лево
     *
     * @param number число, подлежащее проверке на палиндромность
     * @return true, если число является палиндромом, т.е. читается одинаково и слева на право и справа на лево
     */
    public static boolean isPalindrome(int number) {
        StringBuffer buffer = new StringBuffer();
        //Преобразование number из типа int в тип String
        String number_str = String.valueOf(number);
        //Добавление в буфер числа для последующего "разворота" числа
        buffer.append(number_str);
        //Разворачивает содержимое буфера, т.е. переписывает строку справа на лево 
        buffer = buffer.reverse();
        //Возвращает результат сравнения содержимого буфера с числом
        return buffer.toString().equals(number_str);
    }
}
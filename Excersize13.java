/**
 * Решение задачи №12
 * Задача: Из двумерного массива заполненного случайными числами перенесите построчно эти числа в одномерный массив.
 *
 * @author Андрюшин Дмитрий, 16ит18К
 */
public class Excersize13{
    /**
     * Размер квадратной матрицы (двумерного массива)
     */
    private static final int SIZE_OF_MATRIX = 5;

    /**
     * Основной метод класса Main, вызываемый JVM для выполнения программы
     * Решение задачи логически разделено на две части:
     * создание и инициализация матрицы (двумерного массива)
     * запись элементов матрицы в массив
     *
     * @param args массив строк , представляющих аргументы, переданные программе в командной строке
     */
    public static void main(String[] args) {
        int[][] matrix = new int[SIZE_OF_MATRIX][SIZE_OF_MATRIX];

        for(int i = 0; i < matrix.length; i++) {
            for(int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = (int)(Math.random() * 100);
                System.out.printf(matrix[i][j] < 10 ? " %d " : "%d ", matrix[i][j]);
            }
            System.out.println();
        }

        System.out.println();

        int[] array = new int[SIZE_OF_MATRIX * SIZE_OF_MATRIX];
        for(int i = 0; i < array.length; i++) {
            array[i] = matrix[i / SIZE_OF_MATRIX][i % SIZE_OF_MATRIX];
            System.out.printf("%d ", array[i]);
        }
    }
}
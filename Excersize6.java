import java.util.Scanner;

public class Excersize6 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int variable;

        System.out.println("Введите переменную");
        variable = scanner.nextInt();

        for (int i = 1; i <= 10; i++) {
            System.out.println(variable + " * " + i + " = " + variable * i);
        }
    }
}

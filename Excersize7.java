import java.util.Scanner;

public class Excersize7 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите число: ");
        double number = scanner.nextDouble();
        System.out.printf("Это %s число%n", isWhole1(number) ? "целое" : "дробное");
    }

    private static boolean isWhole1(double num) {
        return Math.round(num) == num;
    }

    private static boolean isWhole2(double num) {
        return num % 1.0 == 0.0;
    }
}

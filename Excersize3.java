import java.util.Scanner;

public class Excersize3 {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {


        System.out.print("Введите курс евро: ");
        double course = readCorrect();
        System.out.print("Введите количество рублей: ");
        double roubles = readCorrect();
        System.out.printf("За %.3f рублей можно купить %.3f евро%n", roubles, roubles / course);
    }

    public static double readCorrect() {
        double m = scanner.nextDouble();
        while (m < 0) {
            System.out.print("Вы допустили ошибку! Введите данные еще раз: ");
            m = scanner.nextDouble();
        }
        return m;
    }
}

import java.util.Scanner;

/**
 * Решение задачи №10
 * Требуется написать  метод, определяющий палиндромность строки
 *
 * @author Андрюшин Дмитрий, 16ит18К
 */
public class Excersize10 {
    /**
     * Объект типа {@link java.util.Scanner} позволяет считывать введённые с клавиатуры символы, числа и строки
     */
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Введите строке для проверки на палиндромность (строка может содержать знаки препинания и пробелы, однако они будут игнорироваться): ");
        String string = scanner.nextLine();
        System.out.printf("Строка%sявляется палиндромом!%n", isPalindrome(string) ? " " : " не ");
    }

    /**
     * Возвращает результат сравнения строки с той же строкой, но записанной справа на лево
     *
     * @param string строка, подлежащая проверке на палиндромность
     * @return true, если строка является палиндромом, т.е. читается одинаково и слева на право и справа на лево
     */
    public static boolean isPalindrome(String string) {
        StringBuffer buffer = new StringBuffer();
        string = string.replace("^[a-zA-Z0-9]", "");
        buffer.append(string);
        buffer = buffer.reverse();
        //Возвращает результат сравнения содержимого буфера с числом
        return buffer.toString().equals(string);
    }
}
